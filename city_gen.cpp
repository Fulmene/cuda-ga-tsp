#include <iostream>
#include <random>
#include <chrono>
#include <functional>

int main() {
    const int city_count = 64;
    int dist[city_count][city_count];
    unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed1);
    std::uniform_int_distribution<int> distribution(1, 100);
    auto rand_gen = std::bind(distribution, generator);

    for (int i = 0; i < city_count; i++) {
        for (int j = 0; j < city_count; j++) {
            if (i == j)
                dist[i][j] = 0;
            else if (i > j)
                dist[i][j] = dist[j][i];
            else
                dist[i][j] = rand_gen();
            std::cout << dist[i][j];

            if (j+1 < city_count)
                std::cout << ' ';
            else
                std::cout << std::endl;
        }
    }
}
