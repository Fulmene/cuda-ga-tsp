#include <cstdio>
#include <cmath>
#include <algorithm>

#include <cuda.h>
#include <cuda_runtime.h>
#include <curand_kernel.h>

#define NUM_THREADS 256
#define BLOCKS 4

#define N 64
#define POPULATION_SIZE NUM_THREADS*BLOCKS
#define GENERATIONS 100

#define MUTATION_RATE 0.2
#define TOURNAMENT_SIZE 8

struct phenotype {
    int sequence[N-1];
    int fitness;
};

typedef struct phenotype phenotype_t;

__host__ __device__ phenotype_t new_phenotype(int* sequence) {
    phenotype_t ph;
    for (int i = 0; i < N-1; i++)
        ph.sequence[i] = sequence[i];
    ph.fitness = -1;
    return ph;
}

__global__ void curand_setup(curandState* rand_state) {
    int tid = blockDim.x * blockIdx.x + threadIdx.x;
    if (tid >= POPULATION_SIZE) return;

    curand_init(8000, tid*64, 0, &rand_state[tid]);
}

__global__ void evaluate(phenotype_t* populations, int* distances) {
    int tid = blockDim.x * blockIdx.x + threadIdx.x;
    if (tid >= POPULATION_SIZE) return;

    if (populations[tid].fitness == -1) {
        populations[tid].fitness = distances[populations[tid].sequence[0]] + distances[populations[tid].sequence[N-2] * N];
        for (int j = 0; j < N-2; j++) {
            int from = populations[tid].sequence[j];
            int to = populations[tid].sequence[j+1];
            populations[tid].fitness += distances[from*N + to];
        }
    }

    // if (tid%(POPULATION_SIZE/8) == 0)
    //     printf("%d\n", populations[tid].fitness);
}

__device__ int tournament_select(phenotype_t* populations, curandState* rand_state, int tid) {
    curandState local_state = rand_state[tid];
    int chosen = (1.0 - curand_uniform(&local_state)) * POPULATION_SIZE;
    for (int i = 1; i < TOURNAMENT_SIZE; i++) {
        int participant = (1.0 - curand_uniform(&local_state)) * POPULATION_SIZE;
        if (populations[participant].fitness < populations[chosen].fitness)
            chosen = participant;
    }
    return chosen;
}

__global__ void select(int* parents, phenotype_t* populations, curandState* rand_state) {
    int tid = blockDim.x * blockIdx.x + threadIdx.x;
    if (tid >= POPULATION_SIZE) return;
    
    parents[2*tid] = tournament_select(populations, rand_state, tid);
    parents[2*tid+1] = tournament_select(populations, rand_state, tid);
}

__device__ void sequence_crossover(int parent1, int parent2, phenotype_t* populations, int tid) {
    int seq[N-1];
    int* seq1 = populations[parent1].sequence;
    int* seq2 = populations[parent2].sequence;
    const int mid = N/2;
    int point = mid;

    for (int i = 0; i < mid; i++)
        seq[i] = seq1[i];

    for (int i = 0; i < N-1; i++) {
        bool found = false;
        for (int j = 0; j < mid; j++) {
            if (seq2[i] == seq[j]) {
                found = true;
                break;
            }
        }
        if (!found) {
            seq[point] = seq2[i];
            point++;
        }
    }

    populations[tid] = new_phenotype(seq);
}

__global__ void crossover(int* parents, phenotype_t* populations) {
    int tid = blockDim.x * blockIdx.x + threadIdx.x;
    if (tid >= POPULATION_SIZE) return;

    sequence_crossover(parents[2*tid], parents[2*tid+1], populations, tid);
}

__global__ void mutate(phenotype_t* populations, curandState* rand_state) {
    int tid = blockDim.x * blockIdx.x + threadIdx.x;
    if (tid >= POPULATION_SIZE) return;

    curandState local_state = rand_state[tid];

    if (curand_uniform(&local_state) < MUTATION_RATE) {
        int rand1 = (1 - curand_uniform(&local_state)) * (N-1);
        int rand2 = (1 - curand_uniform(&local_state)) * (N-2);
        if (rand2 >= rand1) rand2 += 1;
        int temp = populations[tid].sequence[rand1];
        populations[tid].sequence[rand1] = populations[tid].sequence[rand2];
        populations[tid].sequence[rand2] = temp;
        populations[tid].fitness = -1;
    }
}

__host__ void check_error()
{
    cudaError_t error_var = cudaGetLastError();
    if(error_var != cudaSuccess)
    {
        printf("CUDA error: %s\n", cudaGetErrorString(error_var));
        exit(-1);
    }
}

int main() {

    int* distances;
    cudaMallocManaged(&distances, N*N*sizeof(int));

    curandState* rand_state;
    cudaMalloc(&rand_state, POPULATION_SIZE*sizeof(curandState));

    phenotype_t* populations;
    cudaMallocManaged(&populations, POPULATION_SIZE*sizeof(phenotype_t));

    int* parents;
    cudaMalloc(&parents, 2*POPULATION_SIZE*sizeof(int));

    cudaDeviceSynchronize();
    check_error();

    phenotype_t pops[POPULATION_SIZE];
    int seq[N-1];
    for (int i = 0; i < N-1; i++)
        seq[i] = i+1;
    for (int i = 0; i < POPULATION_SIZE; i++) {
        std::random_shuffle(seq, seq + (N-1));
        pops[i] = new_phenotype(seq);
    }

    int dist[N*N];
    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            scanf("%d", &dist[i*N+j]);

    cudaMemcpy(distances, dist, N*N*sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(populations, pops, POPULATION_SIZE*sizeof(phenotype_t), cudaMemcpyHostToDevice);

    curand_setup<<< BLOCKS, NUM_THREADS >>>(rand_state);

    cudaDeviceSynchronize();
    check_error();

    float milliseconds;
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start);
    cudaDeviceSynchronize();
    check_error();

    for (int g = 0; g < GENERATIONS; g++) {
        // printf("Generation %d\n", g);

        evaluate<<< BLOCKS, NUM_THREADS >>>(populations, distances);
        select<<< BLOCKS, NUM_THREADS >>>(parents, populations, rand_state);
        crossover<<< BLOCKS, NUM_THREADS >>>(parents, populations);
        mutate<<< BLOCKS, NUM_THREADS >>>(populations, rand_state);

        cudaDeviceSynchronize();
        check_error();
    }

    // printf("Generation %d\n", GENERATIONS);

    evaluate<<< BLOCKS, NUM_THREADS >>>(populations, distances);

    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&milliseconds, start, stop);

    cudaDeviceSynchronize();
    check_error();

    cudaMemcpy(pops, populations, POPULATION_SIZE*sizeof(phenotype_t), cudaMemcpyDeviceToHost);

    cudaDeviceSynchronize();
    check_error();

    int fittest = 0;
    for (int i = 1; i < POPULATION_SIZE; i++)
        if (populations[i].fitness < populations[fittest].fitness)
            fittest = i;

    printf("0");
    for (int i = 0; i < N-1; i++)
        printf(" %d", populations[fittest].sequence[i]);
    printf(" 0 (%d)\n", populations[fittest].fitness);
    printf("%f ms\n", milliseconds);

    cudaFree(parents);
    cudaFree(populations);
    cudaFree(rand_state);
    cudaFree(distances);
}
